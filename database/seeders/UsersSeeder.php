<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name' => 'Admin',
            'email' => 'admin@default.com',
            'password' => bcrypt('123456'),
        ]);

        $admin->assignRole('admin');


        $user = User::create([
            'name' => 'User',
            'email' => 'user@default.com',
            'password' => bcrypt('123456'),
        ]);

        $user->assignRole('user');
        

    }
}
